// 2. Find prime number by range

function findPrimeByRange(min, max) {
  let filter = [], i, j, primes = [];
  for (i = 2; i <= max; ++i) {
    if (!filter[i]) {
      if (i >= min) {
        primes.push(i);
      }
      for (j = i << 1; j <= max; j += i) {
        filter[j] = true;
      }
    }
  }
  return console.log(primes);
}

findPrimeByRange(11, 40);