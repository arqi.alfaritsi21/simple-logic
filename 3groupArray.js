// 3. Grouping arrayay group into separate sub arrayay group

const array = ['a', 'a', 'a', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'e'];

const result = array.reduce((item, index) => {
  if (typeof item.last === 'undefined' || item.last !== index) {
    item.last = index;
    item.array.push([]);
  }
  item.array[item.array.length - 1].push(index);
  return item;
}, { array: [] }).array;

console.log(result);