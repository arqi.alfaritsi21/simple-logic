// 4.  Count same element in an array with format

const array = ['a', 'a', 'a', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'e'];

let map = array.reduce((a, c) => (a[c] = a[c] || 0, a[c]++, a), {});

let output = Object.keys(map).map(s => ([
  map[s],
  s,
]));

console.log(output);