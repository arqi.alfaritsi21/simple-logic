// 1. Check the string is palindrome or not

function isPalindrome(str) {
  var checkWords = Math.floor(str.length / 2);
  for (var i = 0; i < checkWords; i++)
    if (str[i] !== str[str.length - i - 1])
      return console.log(false);
  return console.log(true);
}

isPalindrome("abcba")